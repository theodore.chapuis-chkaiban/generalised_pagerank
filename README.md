# QIP submission in qip.tex

Project structure:

- pythonCode: contains the python code used to generate the data
- results_csv: raw data files used to generate the figures of the article
- results_figures: the figures used in the article
