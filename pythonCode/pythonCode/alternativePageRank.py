# -*- coding: utf-8 -*-
"""
Created on Wed May 12 17:03:13 2021

@author: theod
"""

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import copy
import classical_pagerank as pr
import scipy.sparse as scs
import csv

# Examples of Kernel :
# Heat Kernel of time t, lambda x : np.exp(-t*x)
# Monomial Kernel of power t, lambda x : (1-x)**t
# Inverse Kernel of power t, lambda x : 1/(1+x)**t
# Cosine Kernel of power t, lambda x : np.cos(x*np.pi/4)**t


def altPageRank(matrix, kernel):

    (eigenvalues, eigenvectors) = np.linalg.eig(matrix.T)

    eigenvec = eigenvectors.T

    maxfreq = max(np.real(eigenvalues))

    eigenvec = [[(eigenvec[j][i] * kernel(maxfreq-np.real(eigenvalues[j])))
                 if eigenvec[j][i] != 0 else 0
                 for i in range(len(eigenvec[j]))] for j in range(len(eigenvec))]

    normScores = np.abs(np.sum(eigenvec, axis=0))

    normScores = normScores/np.linalg.norm(normScores, ord=1)

    return (normScores, sorted(np.real(eigenvalues), reverse=True))


def powerMethod(matrix, initial=None, epsilon=10**(-6), maxIter=10000, sparse=False):

    if initial == None:
        initial = np.ones(len(matrix))/len(matrix) if not sparse else np.ones(
            matrix.shape[0])/(matrix.shape[0])  # Initial superposition
        initial = np.array(initial).reshape(-1, 1)

    iterNb = 1

    x0 = matrix @ initial if not sparse else matrix.dot(initial)

    while np.linalg.norm(x0 - initial) > epsilon and iterNb < maxIter:

        initial = x0
        x0 = matrix @ initial if not sparse else matrix.dot(initial)

        iterNb += 1

    if iterNb >= maxIter:
        raise OverflowError("Limit iteration reached")

    return (iterNb)


def convergenceBenchmark(nodeLogStart=2, nodeLogEnd=7, numNodes=10):

    nodes = [int(i)
             for i in (np.logspace(nodeLogStart, nodeLogEnd, num=numNodes))]

    print(nodes)

    val = []
    for i in nodes:
        wg = sparseADSM(i, 10).transpose()
        val.append([i, powerMethod(wg, sparse=True)])
        print(val[-1])

    fig = plt.figure()

    ax = plt.axes()

    x = [val[i][0] for i in range(len(val))]
    z1 = [val[i][1] for i in range(len(val))]
    ax.plot(x, z1)

    plt.show()

    return val


def sparseADSM(nNodes, param, alpha=0.15, gamma=0.65, beta=0.1):

    ADSM = scs.dok_matrix((nNodes, nNodes))

    for (i, j) in nx.to_directed(nx.barabasi_albert_graph(nNodes, param)).edges:

        rand = np.random.rand()

        if rand <= 0.55:  # prob i->j = 0.45
            ADSM[(i, j)] = 1

        if rand >= 0.45:  # prob j->i = 0.45
            ADSM[(j, i)] = 1

    first = [i[0] for i in ADSM.keys()]
    rang = [*range(ADSM.shape[0])]
    dangling = list(set(rang) - set(first))

    ADSMcsr = ADSM.tocsr()

    # Once we have computed the edges, we have to patch the dangling nodes and compute the Stochastic matrix

    ADSMcsrT = ADSMcsr.transpose(copy=True)

    for i in dangling:

        iRowADSM = (gamma*(ADSMcsrT).getrow(i) /
                    (ADSMcsrT.getrow(i).sum())).todok()

        for (j, k) in iRowADSM.keys():

            ADSM[(i, k)] = iRowADSM[(0, k)]

        ADSM[(i, i)] = 1-gamma

    # Update the CSR
    ADSMcsr = ADSM.tocsr()

    # Then we have to compute the stochastic form

    sums = {}

    for (i, j) in ADSM.keys():

        if i not in sums:
            sums[i] = ADSMcsr.getrow(i).sum()

        ADSMcsr[i, j] /= sums[i]

    # Then compute the ADSM version

    sumsT = {}

    ADSMcsrT2 = ADSMcsr.transpose(copy=True)

    for i in range(ADSM.shape[0]):

        sumsT[i] = ADSMcsrT2.getrow(i).sum()

        if sumsT[i] != 0:

            ADSMcsrT2[i] *= (alpha * 1/sumsT[i])
            ADSMcsr[i] *= (1-alpha-beta)

        else:
            ADSMcsr[i] *= (1-beta)

    ADSMcsr += ADSMcsrT2

    diag = scs.csr_matrix(ADSM.shape)

    diag.setdiag([beta for i in range(ADSM.shape[0])])

    ADSMcsr += diag

    return ADSMcsr


def computeRevers(matrix, alpha=0.5, method="direct"):
    revers = copy.deepcopy(np.array(matrix))
    if method == "direct":
        limitProbDist, eigenvalues = altPageRank(matrix, lambda x: 1 if abs(
            x) < 10**(-5) else 0)  # compute PageRank using Google Kernel

        Pi = np.diag(limitProbDist)
        PiSqrt = np.diag(np.sqrt(limitProbDist))

        revers = alpha * revers + \
            (1-alpha) * np.linalg.matrix_power(Pi, -1) @ revers.T @ Pi

        reversNorm = PiSqrt @ revers @ np.linalg.matrix_power(PiSqrt, -1)

        # this loop ensures that the returned reversNorm is symmetric
        for i in range(0, len(reversNorm)):
            for j in range(0, i):
                if abs(reversNorm[i][j] - reversNorm[j][i]) < 10**(-5):
                    reversNorm[i][j] = reversNorm[j][i]
                else:
                    raise ValueError("ReversNorm should be symmetric")

    return (revers, reversNorm, Pi)


class WebGraph():

    def __init__(self, *args, connexComponents=1, directed=True, random=True, method="barabasi_albert"):

        GList = []

        for i in range(connexComponents):
            if method == "personalized":
                GList.append(args)

            elif method == "barabasi_albert":
                (nNodes, param) = args if args else (50, 5)
                GList.append(nx.barabasi_albert_graph(nNodes, param))

            elif method == "complete":
                nNodes = args[0] if args else 10
                GList.append(nx.complete_graph(nNodes))

        self.inputGraph = nx.DiGraph()

        for G in GList:
            self.inputGraph = nx.disjoint_union(self.inputGraph, G)

        self.G = nx.DiGraph()
        self.nNodes = self.inputGraph.number_of_nodes()
        self.adjM = np.zeros((self.nNodes, self.nNodes))

        self.computeAdj(directed, random)

        self.stochM = copy.deepcopy(self.adjM)

        self.computeStochM()

        self.ADSM = None

        self.computeADSM()

        if nNodes < 10000:
            self.reversADSM, self.reversADSMNorm, self.Pi = computeRevers(
                self.ADSM)
            self.Google = None
            self.computeGoogle()

    def draw(self):
        nx.draw_networkx(self.G, pos=nx.kamada_kawai_layout(
            self.G), with_labels=True, font_weight='bold', cmap=plt.get_cmap("Reds"))

    def computeGoogle(self, alpha=0.85):
        self.Google = alpha * self.stochM + \
            (1-alpha) * np.ones((len(self.stochM),
                                 len(self.stochM))) * 1/len(self.stochM)

    def computeAdj(self, directed, random, dangling=2, alpha=0.6):
        for (startN, endN) in list(self.inputGraph.edges):

            if random:
                rand = np.random.rand()

                if rand <= 0.7:  # prob i->j = 2/3
                    self.adjM[startN][endN] = 1
                    self.G.add_edge(startN, endN)
                if rand >= 0.3:  # prob j->i = 2/3
                    self.adjM[endN][startN] = 1
                    self.G.add_edge(endN, startN)

            else:
                self.adjM[startN][endN] = 1
                self.G.add_edge(startN, endN)

                if not directed:
                    self.adjM[endN][startN] = 1
                    self.G.add_edge(endN, startN)

        # Once we have computed the edges, we have to patch the dangling nodes

        # First dangling patching method, only add self-looping edges
        if(dangling == 1):
            for i in range(0, len(self.adjM)):
                if sum(self.adjM[i]) == 0:
                    self.adjM[i][i] = 1

                    # We do not reflect the patch on the original graph G, to be able to compute
                    # The classical PageRank

        # Second dangling patching method, reverse the existing edges and add self-looping
        elif(dangling == 2):
            for i in range(0, len(self.adjM)):

                if sum(self.adjM[i]) == 0:

                    print("dang")
                    s = (sum(np.array(self.adjM).T[i]))
                    self.adjM[i] = alpha*(np.array(self.adjM).T)[i]/s
                    self.adjM[i][i] = 1-alpha

                    print(sum(self.adjM[i]))

    def computeStochM(self):
        for lineN in range(self.nNodes):  # normalize stochM

            line = self.stochM[lineN]

            if sum(line) == 0:
                print(lineN)

            self.stochM[lineN] = line/sum(line)

    def computeADSM(self, type=1, graphExt=None, alpha=0.15, beta=0.10):

        self.ADSM = copy.deepcopy(self.stochM)

        if type == 1:
            if not graphExt:
                stochMT = self.stochM.T
            else:
                stochMT = graphExt.stochM.T

            for i in range(self.nNodes):

                self.ADSM[i] *= (1-alpha-beta)
                self.ADSM[i] += alpha*stochMT[i] / \
                    sum(stochMT[i]) if sum(stochMT[i]) != 0 else 0
                self.ADSM[i][i] += beta

        if type == 2:
            if not graphExt:
                AdjT = self.adjM.T
            else:
                AdjT = graphExt.adjM.T

            for i in range(self.nNodes):
                if sum(AdjT[i]) != 0:

                    self.ADSM[i] *= (1-alpha)
                    self.ADSM[i] += alpha*AdjT[i]/sum(AdjT[i])

                elif sum(self.ADSM[i]) == 0:
                    self.ADSM[i] += AdjT[i]/sum(AdjT[i])


class PageRank():

    def __init__(self, inputGraph=None):
        if not inputGraph:
            inputGraph = WebGraph()
        self.Gclass = inputGraph
        self.G = inputGraph.G
        self.I = np.zeros(inputGraph.G.number_of_nodes())
        self.eigenvalues = []

    def __call__(self, method="classical", **kwargs):

        if method == "classical":
            if kwargs:
                self.I = pr.pagerank(self.G, kwargs)
            else:
                self.I = pr.pagerank(self.G)

        elif method == "alternative":
            if 'matrix' not in kwargs:
                kwargs['matrix'] = self.Gclass.ADSM
            if 'kernel' not in kwargs:
                kwargs['kernel'] = lambda x: 1 if abs(x) < 10**(-5) else 0

            self.I, self.eigenvalues = altPageRank(
                kwargs['matrix'], kwargs['kernel'])

    def drawResults(self, inputType="dic", draw_simple=True):

        if inputType == "dic":

            colors = np.zeros(self.Gclass.nNodes)

            for key in self.I.keys():

                colors[key] = self.I[key]

            nx.draw_networkx(self.G, pos=nx.kamada_kawai_layout(
                self.G), with_labels=True, font_weight='bold', node_color=colors, cmap=plt.get_cmap("Reds"))

        if inputType == "array":
            if draw_simple:
                nx.draw_networkx(self.G, pos=nx.kamada_kawai_layout(self.G),
                                 with_labels=True, font_weight='bold',
                                 nodelist=range(len(self.G.nodes())),
                                 node_color=self.I, label="Node Number",
                                 cmap=plt.get_cmap("Reds"))

            else:
                nx.draw_networkx(self.G, pos=nx.kamada_kawai_layout(self.G),
                                 with_labels=True, font_weight='bold',
                                 nodelist=range(len(self.G.nodes())),
                                 labels={
                                     i: str(i) + " | " + str(round(self.I[i], 2)) for i in range(len(self.I))},
                                 font_size=6, node_size=1000,
                                 node_color=self.I, vmin=0, label="Node Number | Score",
                                 vmax=1, cmap=plt.get_cmap("Reds"))

    def buildAndCompareResults(self, kernelList=[{"function": lambda x: 1 if abs(x) < 10**(-3) else 0, "comment": "Classical Google Kernel"}], matrixList=None, cols=2, export_csv_nodes=None, export_csv_rankings=None,
                               export_fig=None, plot_scores="bar", draw_simple=True, **kwargs):

        if export_csv_nodes:
            csv_file_nodes = open(export_csv_nodes, 'w', newline='')
            writer_nodes = csv.writer(csv_file_nodes, delimiter=';')
            fields = ["Node Number", "Ranking Classical PageRank",
                      "Score Classical PageRank"]

            for i in range(len(kernelList)):
                fields += ["Ranking " + kernelList[i]["comment"],
                           "Score " + kernelList[i]["comment"]]
            writer_nodes.writerow(fields)

        if export_csv_rankings:
            csv_file_rankings = open(export_csv_rankings, 'w', newline='')
            writer_rankings = csv.writer(csv_file_rankings, delimiter=';')
            fields = ["Ranking", "Node Number Classical PageRank",
                      "Score Classical PageRank"]
            for i in range(len(kernelList)):
                fields += ["Node Number " + kernelList[i]
                           ["comment"], "Score " + kernelList[i]["comment"]]
            writer_rankings.writerow(fields)

        nodes_order = []
        rankings_order = []

        if matrixList == None:
            matrixList = [
                {"matrix": self.Gclass.reversADSM, "comment": "Reversed ADSM"}]

        # First compute the number of rows for the plot
        rows = int(1 + np.ceil((len(kernelList)*len(matrixList)+1)/cols))

        # Start plotting the classical based results

        self(method="classical")

        resultsClass = np.zeros(len(self.I))

        for i in self.I.keys():
            resultsClass[i] = self.I[i]

        self.I = (np.array(resultsClass) - np.min(resultsClass)) / \
            (np.max(resultsClass) - np.min(resultsClass))

        curr_ranking = [[i, self.I[i]] for i in range(len(self.I))]
        curr_ranking = sorted(curr_ranking, key=lambda x: x[1], reverse=True)

        rankings_order = [([i+1] + curr_ranking[i])
                          for i in range(len(curr_ranking))]

        nodes_order = sorted(rankings_order, key=lambda x: x[1])
        nodes_order = [[nodes_order[i][1], nodes_order[i][0],
                        nodes_order[i][2]] for i in range(len(nodes_order))]

        plt.subplot(rows, cols, 3)  # Plot the graph
        plt.xlabel("Classical PageRank")
        self.drawResults(inputType="array", draw_simple=draw_simple)

        plt.subplot(rows, cols, 1)  # Plot the importance vector
        plt.title("Generalized Importance Vector")
        plt.ylabel("Importance score")

        if plot_scores == "line":
            plt.xlabel("Ranking")
            plt.plot(sorted(self.I, reverse=True), label="Classical PageRank")
        elif plot_scores == "bar":
            plt.xlabel("Node number")
            nb_plot = len(kernelList)*len(matrixList)+1
            offset = 0.75/(nb_plot)
            plt.bar(np.arange(len(self.I)) + (offset*(nb_plot//2 - 1) + offset/2),
                    height=self.I, label="Classical PageRank", width=offset)

        plt.legend(loc="upper right")

        plt.subplot(rows, cols, 2)  # Plot the Kernel distribution
        plt.title("Kernel distribution for generalized PageRank")
        plt.xlabel("Eigenvalue Number")
        plt.ylabel("Kernel Value")

        plt.plot(np.concatenate(
            ([1], np.zeros(len(self.Gclass.ADSM)))), label="Classical PageRank")

        plt.legend(loc="upper right")

        # Then loop and plot each graph in the list
        for i in range(0, len(kernelList)):

            for j in range(0, len(matrixList)):

                self(method="alternative", **
                     {"matrix": matrixList[j]["matrix"], "kernel": kernelList[i]["function"]})

                self.I = (np.array(self.I) - np.min(self.I)) / \
                    (np.max(self.I) - np.min(self.I))

                plt.subplot(rows, cols, (i+1)*(j+1)+3)

                plt.xlabel(kernelList[i]["comment"] +
                           "; " + matrixList[j]["comment"])
                self.drawResults(inputType="array", draw_simple=draw_simple)

                plt.subplot(rows, cols, 1)

                if plot_scores == "line":
                    plt.plot(sorted(self.I, reverse=True),
                             label=kernelList[i]["comment"] + "; " + matrixList[j]["comment"])
                elif plot_scores == "bar":
                    plt.bar(np.arange(len(self.I)) + ((nb_plot//2 - 1 - (i+1)*(j+1))*offset + offset/2),
                            height=self.I, label=kernelList[i]["comment"] + "; " + matrixList[j]["comment"], width=offset)

                plt.legend(loc="upper right")

                plt.subplot(rows, cols, 2)

                plt.plot([kernelList[i]["function"](1-np.real(eigenvalue)) for eigenvalue in self.eigenvalues],
                         label=kernelList[i]["comment"] + "; " + matrixList[j]["comment"])

                plt.legend(loc="upper right")

                curr_ranking = [[i, self.I[i]] for i in range(len(self.I))]
                curr_ranking = sorted(
                    curr_ranking, key=lambda x: x[1], reverse=True)

                curr_nodes = [[index, item[1]]
                              for index, item in enumerate(curr_ranking)]

                for k in range(len(nodes_order)):
                    nodes_order[k] += curr_nodes[k]

                for k in range(len(rankings_order)):
                    rankings_order[k] += curr_ranking[k]

        if export_fig:
            plt.savefig(export_fig, dpi=500, bbox_inches="tight")
        if export_csv_nodes:
            writer_nodes.writerows(nodes_order)
            csv_file_nodes.close()
        if export_csv_rankings:
            writer_rankings.writerows(rankings_order)
            csv_file_rankings.close()
